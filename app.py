from tornado.web import Application, RequestHandler
from tornado.ioloop import IOLoop
from sqlalchemy import create_engine, orm
import json

from database_model import Numbers


class MainHandler(RequestHandler):
    def initialize(self, db):
        self.session = orm.Session(bind=db)

    def get(self):
        ident = json.loads(self.request.body).get('id')
        if self.validate_get_req(ident):
            data = self.session.query(Numbers).get(ident=ident)
            response = {
                'id': data.id,
                'numbers': data.numbers,
                'sorted_numbers': data.sorted_numbers,
                'date': data.date.strftime("%Y-%m-%d-%H.%M.%S")
            }
            self.write({'result': response})
        else:
            self.write({'result': 'ERROR', 'error_message': 'Invalid arguments'})
        self.session.close()

    def post(self):
        numbers = json.loads(self.request.body).get('numbers')
        if self.validate_numbers(numbers):
            self.add_numbers(numbers)
            self.write({'result': numbers})
        else: 
            self.write({'result': 'ERROR', 'error_message': 'Invalid arguments'})
        self.session.close()

    def add_numbers(self, numbers: list):
        sorted_numbers = sorted(numbers)
        self.session.add(Numbers(
            numbers=numbers,
            sorted_numbers=sorted_numbers,
        ))
        self.session.commit()

    def validate_get_req(self, ident):
        if not isinstance(ident, int):
            return False
        elif self.session.query(Numbers).get(ident=ident) is None:
            return False
        return True

    def validate_numbers(self, numbers):
        if not isinstance(numbers, list):
            return False
        for i in numbers:
            if not isinstance(i, int):
                return False
        return True


def make_app():
    db = create_engine('postgresql://postgres:123123@localhost:5431')
    urls = [('/', MainHandler, dict(db=db))]
    return Application(urls)


def start():
    app = make_app()
    app.listen(3000)
    IOLoop.instance().start()


if __name__ == '__main__':
    start()
