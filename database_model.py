from sqlalchemy import Column, DateTime, Integer, func, ARRAY, MetaData
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = MetaData()


class Numbers(Base):
    __tablename__ = 'numbers'
    id = Column(Integer, primary_key=True, autoincrement=True)
    numbers = Column(ARRAY(Integer))
    sorted_numbers = Column(ARRAY(Integer))
    date = Column(DateTime, default=func.now())
